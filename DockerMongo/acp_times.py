"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

BREVET_SPEEDS = {(0, 200): (15, 34), (200, 400): (15, 34), (400, 600): (15, 30), (600, 1000): (11.428, 28), (1000, 1300): (13.333, 26)} 


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    for i in BREVET_SPEEDS:
        if i[0] < brevet_dist_km <= i[1]:
            max_speed = BREVET_SPEEDS[i][1]
    time = brevet_start_time.shift(hours=control_dist_km/max_speed)
    return time


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    for i in BREVET_SPEEDS:
        if i[0] < brevet_dist_km <= i[1]:
            min_speed = BREVET_SPEEDS[i][0]
    time = brevet_start_time.shift(hours=control_dist_km/min_speed)
    return time
