"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import os

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb
###
# Pages
###


@app.route("/times")
def times():
    time_table = {}
    for i, times in enumerate(db.times.find()):
        del times['_id']
        time_table["row_" + str(i)] = times
    return flask.render_template("times.html", times=time_table)


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_submit")
def _submit():
    db.times.drop()
    get = request.args.get
    for i in range(1,int(get("length"))):
        db.times.insert_one({
            "miles": get("miles" + str(i)),  
            "km": get("km" + str(i)),
            "location": get("location" + str(i)),
            "open": get("open" + str(i)),
            "close": get("close" + str(i))
        })
    return flask.jsonify(result={})

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 200, type=float)
    time = request.args.get('time', "00:00", type=str)
    date = request.args.get('date', "2017-01-01", type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    arrow_time = arrow.get(date + " " + time, "YYYY-MM-DD HH:mm")
    open_time = acp_times.open_time(km, dist, arrow_time)
    close_time = acp_times.close_time(km, dist, arrow_time)
    result = {"open": open_time.isoformat(), "close": close_time.isoformat()}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
