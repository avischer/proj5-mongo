Author: Alex Vischer
eMail: avischer@uoregon.edu

In order to use this amazing program, simply go to your favorite folder and type into
the command line:

git clone https://avischer@bitbucket.org/avischer/proj5-mongo.git

Now go into the DockerMongo directory and type "docker-compose up" without the 
quotation marks.

The logic in this project is pretty simple:

1. User enters brevet distance and start time on front page.
2. User submits an arbitrary number of controle distances into the input box, which are sent to the backend with AJAX and then sent to the database from flask.
3. Upon clicking display, the webpage requests all collections from the database that are part of the brevet field and formats them into a beatifully readable page.

I do realize that we need to handle error cases appropriately, and this application
assumes that the users know what they are doing, hence minimal error-safe-gaurds.

Now you will be able to compute all the brevet times you could ever need!
